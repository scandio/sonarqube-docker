#!/bin/bash

# If configuration files do not exist because an empty conf
# directory was mapped from the host, copy the standard files
if [ ! -f ${SONARQUBE_HOME}/conf/sonar.properties ]; then
	cp ${SONARQUBE_HOME}/conftemplate/sonar.properties ${SONARQUBE_HOME}/conf/
fi

if [ ! -f ${SONARQUBE_HOME}/conf/wrapper.conf ]; then
	cp ${SONARQUBE_HOME}/conftemplate/wrapper.conf ${SONARQUBE_HOME}/conf/
fi

# Set JDBC options in sonar.properties file
JDBC_URL_ESCAPED=$(echo $JDBC_URL | perl -pe 's/(?<!\\)&/\\&/g')
sed -i -e "s|^#*\(sonar.jdbc.username=\).*$|\1${JDBC_USERNAME}|" \
	-e "s|^#*\(sonar.jdbc.password=\).*$|\1${JDBC_PASSWORD}|" \
	-e "s|^#*\(sonar.jdbc.url=\)jdbc:mysql.*$|\1${JDBC_URL_ESCAPED}|" \
	${SONARQUBE_HOME}/conf/sonar.properties

# Set HTTP Proxy options in sonar.properties file
if [ ${HTTP_PROXYHOST} != "false" ] && [ ${HTTP_PROXYPORT} != "false" ]; then
	sed -i -e "s|^#*\(http.proxyHost=\).*$|\1${HTTP_PROXYHOST}|" \
		-e "s|^#*\(http.proxyPort=\).*$|\1${HTTP_PROXYPORT}|" \
		${SONARQUBE_HOME}/conf/sonar.properties
	if [ ${HTTP_PROXYUSER} != "false" ] && [ ${HTTP_PROXYPASSWORD} != "false" ]; then
		sed -i -e "s|^#*\(http.proxyUser=\).*$|\1${HTTP_PROXYUSER}|" \
			-e "s|^#*\(http.proxyPassword=\).*$|\1${HTTP_PROXYPASSWORD}|" \
			${SONARQUBE_HOME}/conf/sonar.properties
	fi
else
	sed -i -e "s|^\(http.proxyHost=\).*$|#\1|" \
		-e "s|^\(http.proxyPort=\).*$|#\1|" \
		-e "s|^\(http.proxyUser=\).*$|#\1|" \
		-e "s|^\(http.proxyPassword=\).*$|#\1|" \
		${SONARQUBE_HOME}/conf/sonar.properties
fi

# Import SSL certificates
if [ ${IMPORTCERT} = "true" ]; then 
        CATALINA_OPTS="-Djavax.net.ssl.trustStore=/etc/ssl/certs/java/cacerts -Djavax.net.ssl.trustStorePassword=changeit $CATALINA_OPTS"     

        cd ${IMPORTPATH}
        if [ ! -f ${SONARQUBE_HOME}/skipcert.conf ]; then
                for i in *
                do  
                        /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/keytool -keystore /etc/ssl/certs/java/cacerts -importcert -alias $i -file $i -storepass changeit -noprompt
                done
                touch ${SONARQUBE_HOME}/skipcert.conf
        fi  
fi

#Start SonarQube server
cd ${SONARQUBE_HOME}
exec java -jar lib/sonar-application-$SONAR_VERSION.jar -Dsonar.log.console=true -Dsonar.web.javaAdditionalOpts="-Djava.security.egd=file:/dev/./urandom"

